import argparse
import logging
from logging import config

import settings
from parser import parser


def main():
    """
    Gets the input from the user. Calls the parser module, and returns
     the result
    """
    cli_parser = argparse.ArgumentParser()
    cli_parser.add_argument("-f", nargs=1,
                            help="Specifies the source csv file. E.g:"
                                 " %(metavar)s", required=True,
                            metavar="cookie_log.csv")
    cli_parser.add_argument("-d", nargs=1,
                            help="Specifies the target date. E.g: %(metavar)s",
                            required=True, metavar="2018-12-09")
    args = cli_parser.parse_args()
    if not args.f[0].endswith(".csv"):
        raise ValueError("input file must be in csv format")

    result = parser.parse(source_file=args.f[0], target_date=args.d[0])
    logging.getLogger().log(level=logging.INFO, msg="\n" + "\n".join(result))


if __name__ == "__main__":
    logging.config.dictConfig(settings.LOGGING_CONFIG)
    cli_logger = logging.getLogger(__name__)
    main()
