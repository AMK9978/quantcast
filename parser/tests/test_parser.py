import datetime
import os

import pytest
from faker import Faker

from parser import parser

Faker.seed(0)
fake = Faker()


def get_random_datetime(length: int) -> set:
    """
    :param length: the number of datetimes to be generated
    :return: set of unique datetimes based on the specified format
    """
    random_dates = set()
    for i in range(length):
        random_datetime: datetime.datetime = fake.date_time()
        result = "{}T{}+00:00".format(random_datetime.date(),
                                      random_datetime.time())
        while result in random_dates:
            random_datetime: datetime.datetime = fake.date_time()
            result = "{}T{}+00:00".format(random_datetime.date(),
                                          random_datetime.time())

        random_dates.add(result)
    return random_dates


@pytest.fixture()
def source_test_file():
    """
    pytest fixture for working with the test source file. I preferred
     this way to using tempfile module or tmpfile fixture.
    """
    source_test_file = open('temp.csv', 'w')
    source_content = 'cookie,timestamp\n'
    source_test_file.writelines(source_content)
    source_test_file.flush()
    yield source_test_file
    source_test_file.flush()
    source_test_file.close()
    os.remove(source_test_file.name)


def test_get_date():
    input_datetime = "1996-03-20T07:46:39+00:00"
    assert parser.get_date(datetime=input_datetime) == "1996-03-20"


def test_parse_same_cookie_same_date(source_test_file):
    content = "LicnwVrvjCnETFGg,1990-08-22T21:05:08+00:00"
    source_test_file.writelines(content)
    source_test_file.flush()
    assert parser.parse(source_file=source_test_file.name,
                        target_date="1990-08-22") == ["LicnwVrvjCnETFGg"]


def test_parse_two_cookies_same_date(source_test_file):
    cookie = "LicnwVrvjCnETFGg"
    cookie2 = "QQcnwVrvjCnETFGg"
    cookie_dates = []
    date = "1990-08-22T21:05:08+00:00"
    for i in range(10):
        if i < 5:
            cookie_dates.append("{},{}".format(cookie, date))
        else:
            cookie_dates.append("{},{}".format(cookie2, date))

    source_test_file.writelines("\n".join(cookie_dates))
    source_test_file.flush()
    assert parser.parse(source_file=source_test_file.name,
                        target_date="1990-08-22") == [cookie, cookie2]


def test_different_cookies_different_dates(source_test_file):
    cookie = "LicnwVrvjCnETFGg"
    cookie2 = "QQcnwVrvjCnETFGg"
    random_dates = get_random_datetime(10)
    cookie_dates = []
    target_date = random_dates.pop()
    random_dates.add(target_date)
    for i in range(10):
        if i < 5:
            cookie_dates.append("{},{}".format(cookie, random_dates.pop()))
        else:
            cookie_dates.append("{},{}".format(cookie2, random_dates.pop()))

    source_test_file.writelines("\n".join(cookie_dates))
    source_test_file.writelines(
        "\n".join(["{},{}".format(cookie, target_date)]))
    source_test_file.writelines(
        "\n".join(["{},{}".format(cookie2, target_date)]))
    source_test_file.flush()
    assert parser.parse(source_file=source_test_file.name,
                        target_date=parser.get_date(datetime=target_date)) == [
               cookie2]


def test_different_cookies_different_dates_returns_empty(source_test_file):
    cookie = "LicnwVrvjCnETFGg"
    cookie2 = "QQcnwVrvjCnETFGg"
    random_dates = get_random_datetime(11)
    cookie_dates = []
    for i in range(10):
        if i < 5:
            cookie_dates.append("{},{}".format(cookie, random_dates.pop()))
        else:
            cookie_dates.append("{},{}".format(cookie2, random_dates.pop()))

    source_test_file.writelines("\n".join(cookie_dates))
    source_test_file.flush()
    assert parser.parse(source_file=source_test_file.name,
                        target_date=parser.get_date(
                            datetime=random_dates.pop())) == []
