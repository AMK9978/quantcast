import csv
from collections import defaultdict


def get_date(datetime: str):
    return datetime.split("T")[0]


def get_cookies_frequency(source_file: str, target_date: str) -> defaultdict:
    """
    Reads the input file as a csv file. As the target date is
    pre-specified, it just looks for the cookies in that date.
    :param source_file:
    :param target_date:
    :return:
    """
    cookie_frequency = defaultdict(int)
    with open(source_file, 'r') as file:
        csvreader = csv.reader(file)
        next(csvreader, None)
        for row in csvreader:
            date = get_date(row[1])
            if date != target_date:
                continue
            cookie_frequency[row[0]] += 1
    return cookie_frequency


def parse(source_file: str, target_date: str) -> list:
    """
    Gets the cookies frequency in the given date.
    :param source_file: the source csv file
    :param target_date: the date we are investigating
    :return: A list of most frequent cookies in the given date
    """
    cookies_frequency: defaultdict[int] = get_cookies_frequency(
        source_file=source_file,
        target_date=target_date)
    maximum, keys = 0, []
    for k, v in cookies_frequency.items():
        if v > maximum:
            maximum = v
            keys = [k]
        elif v == maximum:
            keys.append(k)
    return keys
