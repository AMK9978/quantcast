LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'std_out': {
            'format': '%(levelname)s %(asctime)-7s [%(name)s:%(funcName)s]'
                      ' %(message)s'
        }
    },
    'handlers': {
        'stream': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': "std_out",
        }
    },
    'root': {
        'handlers': ['stream'],
        'level': 'DEBUG',
    },
    'loggers': {
        'updater': {
            'handlers': ['stream'],
            'propagate': False,
            'level': 'DEBUG',
        }
    }
}
