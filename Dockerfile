FROM python:3.8-slim-buster


ADD requirements.txt /root/
RUN pip install -r /root/requirements.txt

ARG SOURCE_FILE
ADD ${SOURCE_FILE} /usr/quantcast/

ADD . /usr/quantcast/
