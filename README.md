# Quantcast CLI


## Get started

### Using Docker:
You can set your own source csv file as the input file (though consider 
docker constraints for COPY and ADD commands like the file should exist in the
build context).
```
git clone https://gitlab.com/AMK9978/qunatcast.git
cd qunatcast
SOURCE_FILE=source.csv
docker build -t quantcast --build-arg SOURCE_FILE=$SOURCE_FILE .
docker run -d quantcast python /usr/quantcast/main.py -f /usr/quantcast/source.csv -d 2018-12-08
```

### Without Docker (python3.8 and pip are installed as the prerequisites):
```
git clone https://gitlab.com/AMK9978/qunatcast.git
cd Qunatcast
pip installl -r requirements.txt
python3 main.py -f source.csv -d 2018-12-08
```

##Structure
This project is a simple modular project, consisted of a main file containing
the CMD receiver from the outside, and then runs the parser module for parsing,
and executing the intended use-case, which is finding the most used cookie in 
the specified date with the format used in the source.csv as an example. Plus,
there are some tests for the parser module. As this project is super simple,
there was no need to create abstractions and using OOP concepts such as
inheritance or extending. This project is based on some function which can 
convert into methods of a class with the later use-cases. By the way, comments
are added to the functions as documentation. Moreover, keep in mind that 
writing comment next to the definition of the functions, is not a bad practice!


## Used libraries
As you can see in the requirements.txt file, the only essential libraries used
in this project are pytest and faker, both for writing and running the written
tests.


## Example
Input:
```
python3 main.py -d 2018-12-08 -f source.csv
```
Output:
``` 
INFO 2023-01-17 01:02:00,077 [root:main] 
SAZuXPGUrfbcn5UA
4sMM2LxV07bPJzwf
fbcn5UAVanZf6UtG
```